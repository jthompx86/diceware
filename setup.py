from distutils.core import setup

setup(
    name = 'dice-gen',
    license='MIT',
    version='0.0.1',
    author = 'JoTho',
    author_email = 'jthomp@protonmail.com',
    url='https://github.com/diceware',
    description = 'Simple diceware python generator')
